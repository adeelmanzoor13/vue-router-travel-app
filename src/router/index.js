import homePage from "@/views/Home.vue"
// import aboutPage from "@/views/About.vue"
import {createRouter, createWebHistory} from "vue-router"

const routes=[

    {path:'/', name:'Home', component:homePage},
    // {path:'/about', name:'About', component:()=>import('@/views/About.vue')},
    {path:'/brazil', name:'Brazil', component:()=>import('@/views/Brazil.vue')},
    {path:'/jamaica', name:'Jamaic', component:()=>import('@/views/Jamaica.vue')},
    {path:'/hawaii', name:'Hawaii', component:()=>import('@/views/Hawaii.vue')},
    {path:'/panama', name:'Panama', component:()=>import('@/views/Panama.vue')},
    {path:'/destination/:id',  component:()=>import('@/views/DestinationShow.vue')}

]

const router=createRouter({
    history:createWebHistory(),routes
 })
export default router